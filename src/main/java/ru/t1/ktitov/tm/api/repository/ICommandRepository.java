package ru.t1.ktitov.tm.api.repository;

import ru.t1.ktitov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    void add(final AbstractCommand command);

    AbstractCommand getCommandByArgument(final String argument);

    AbstractCommand getCommandByName(final String name);

    Collection<AbstractCommand> getTerminalCommands();

}
