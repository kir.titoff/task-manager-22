package ru.t1.ktitov.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}
