package ru.t1.ktitov.tm.command.task;

import ru.t1.ktitov.tm.model.Task;
import ru.t1.ktitov.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    public static final String NAME = "task-show-by-id";

    public static final String DESCRIPTION = "Show task by id";

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        final Task task = getTaskService().findOneById(userId, id);
        showTask(task);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
