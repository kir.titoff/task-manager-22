package ru.t1.ktitov.tm.command.project;

public final class ProjectClearCommand extends AbstractProjectCommand {

    public static final String NAME = "project-clear";

    public static final String DESCRIPTION = "Remove all projects";

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        final String userId = getUserId();
        getProjectService().clear(userId);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
