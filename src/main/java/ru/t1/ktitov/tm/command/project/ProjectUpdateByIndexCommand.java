package ru.t1.ktitov.tm.command.project;

import ru.t1.ktitov.tm.util.TerminalUtil;

public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    public static final String NAME = "project-update-by-index";

    public static final String DESCRIPTION = "Update project by index";

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT BY INDEX");
        System.out.print("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.print("ENTER NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.print("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        final String userId = getUserId();
        getProjectService().updateByIndex(userId, index, name, description);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
