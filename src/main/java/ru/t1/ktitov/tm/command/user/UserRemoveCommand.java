package ru.t1.ktitov.tm.command.user;

import ru.t1.ktitov.tm.enumerated.Role;
import ru.t1.ktitov.tm.util.TerminalUtil;

public class UserRemoveCommand extends AbstractUserCommand {

    public static final String NAME = "user-remove";

    public static final String DESCRIPTION = "Remove user";

    @Override
    public void execute() {
        System.out.println("[REMOVE USER]");
        System.out.print("ENTER LOGIN: ");
        final String login = TerminalUtil.nextLine();
        getUserService().removeByLogin(login);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
