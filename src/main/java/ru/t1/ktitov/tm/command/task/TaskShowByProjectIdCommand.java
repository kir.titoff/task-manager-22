package ru.t1.ktitov.tm.command.task;

import ru.t1.ktitov.tm.model.Task;
import ru.t1.ktitov.tm.util.TerminalUtil;

import java.util.List;

public final class TaskShowByProjectIdCommand extends AbstractTaskCommand {

    public static final String NAME = "task-show-by-project-id";

    public static final String DESCRIPTION = "Show task by project id";

    @Override
    public void execute() {
        System.out.println("[SHOW TASKS BY PROJECT ID]");
        System.out.print("ENTER PROJECT ID: ");
        final String projectId = TerminalUtil.nextLine();
        final String userId = getUserId();
        final List<Task> tasks = getTaskService().findAllByProjectId(userId, projectId);
        renderTasks(tasks);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
