package ru.t1.ktitov.tm.command.user;

import ru.t1.ktitov.tm.enumerated.Role;
import ru.t1.ktitov.tm.model.User;
import ru.t1.ktitov.tm.util.TerminalUtil;

public final class UserRegistryCommand extends AbstractUserCommand {

    public static final String NAME = "user-registry";

    public static final String DESCRIPTION = "Registry user";

    @Override
    public void execute() {
        System.out.println("[REGISTRY USER]");
        System.out.print("ENTER LOGIN: ");
        final String login = TerminalUtil.nextLine();
        System.out.print("ENTER PASSWORD: ");
        final String password = TerminalUtil.nextLine();
        System.out.print("ENTER EMAIL: ");
        final String email = TerminalUtil.nextLine();
        final User user = getAuthService().registry(login, password, email);
        System.out.println("NEW USER:");
        showUser(user);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
