package ru.t1.ktitov.tm.command.user;

import ru.t1.ktitov.tm.enumerated.Role;
import ru.t1.ktitov.tm.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    public static final String NAME = "user-update-profile";

    public static final String DESCRIPTION = "Update profile of current user";

    @Override
    public void execute() {
        System.out.println("[UPDATE USER PROFILE]");
        System.out.print("ENTER FIRST NAME: ");
        final String firstName = TerminalUtil.nextLine();
        System.out.print("ENTER LAST NAME: ");
        final String lastName = TerminalUtil.nextLine();
        System.out.print("ENTER MIDDLE NAME: ");
        final String middleName = TerminalUtil.nextLine();
        final String userId = getAuthService().getUserId();
        getUserService().updateUser(
                userId, firstName, lastName, middleName
        );
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
