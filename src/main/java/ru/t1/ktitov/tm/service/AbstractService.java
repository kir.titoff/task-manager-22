package ru.t1.ktitov.tm.service;

import ru.t1.ktitov.tm.api.repository.IRepository;
import ru.t1.ktitov.tm.api.service.IService;
import ru.t1.ktitov.tm.enumerated.Sort;
import ru.t1.ktitov.tm.exception.entity.EntityNotFoundException;
import ru.t1.ktitov.tm.exception.field.EmptyIdException;
import ru.t1.ktitov.tm.exception.field.IncorrectIndexException;
import ru.t1.ktitov.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    protected final R repository;

    public AbstractService(final R repository) {
        this.repository = repository;
    }

    @Override
    public M add(final M model) {
        if (model == null) throw new EntityNotFoundException();
        return repository.add(model);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    public List<M> findAll(final Comparator comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Override
    public List<M> findAll(final Sort sort) {
        if (sort == null) return findAll();
        return findAll(sort.getComparator());
    }

    @Override
    public boolean existsById(final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Override
    public M findOneById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final M model = repository.findOneById(id);
        if (model == null) throw new EntityNotFoundException();
        return model;
    }

    @Override
    public M findOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        final M model = repository.findOneByIndex(index);
        if (model == null) throw new EntityNotFoundException();
        return model;
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

    @Override
    public M remove(final M model) {
        if (model == null) throw new EntityNotFoundException();
        return repository.remove(model);
    }

    @Override
    public M removeById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.removeById(id);
    }

    @Override
    public M removeByIndex(final Integer index) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        return repository.removeByIndex(index);
    }

}
